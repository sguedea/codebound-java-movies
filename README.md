# INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here.




We are going to build an application that keeps track of movies and displays them by category:
* You've been provided a directory name Movies. 
* Create a class name Movie. This class should have private fields for name and category, and a constructor that sets both of these. Create methods to access these properties and change them (getters and setters).
* The directory contains a class named MoviesArray.java. This class has a static method named findAll() that returns the array of Movie objects.
* Create a class named MovieApp. This class should contain a main method.
* Give the user a list of options to view all movies or view the movies by category.
* If a category is selected, display all the movies that contains that category.
* Your application should continue until the user decides to exit.
*  Example of how it should look like.
         *  What would you like to see?
         *  0 - exit
         *  1 - View all Movies
         *  2 - Comedy
         *  3 - Drama
         *  4 - Animated
         *  5 - Horror
         *  6 - Sci-fi
         *  Enter your selection:
         
         *  List of Movies will be displayed here
         


# BONUS
* Add a feature that will allow the user to add a new movie to the list.

